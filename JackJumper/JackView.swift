//
//  JackView.swift
//  JackJumper
//
//  Created by V.K. on 2/22/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class JackView: UIView {
    
    override func draw(_ rect: CGRect) {
        if let context = UIGraphicsGetCurrentContext() {
            context.addArc(
                center: CGPoint(x: bounds.midX, y: bounds.midY),
                radius: min(bounds.midX, bounds.midY),
                startAngle: 0,
                endAngle: CGFloat.pi * 2,
                clockwise: true
            )
            context.setFillColor(#colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1))
            context.fillPath()
        }
        

    }
    
}
