//
//  ChamberTwoView.swift
//  JackJumper
//
//  Created by V.K. on 2/25/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ChamberTwoView: UIView {
    
    let path = UIBezierPath()
    let safePath = UIBezierPath()
    
    override func draw(_ rect: CGRect) {
        let N: Int = 8
        let R = min(bounds.width, bounds.height) * 0.5 - 5.0
        let safeRatio = CGFloat(0.82)
                
        let boxColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        boxColor.setStroke()
        let da = 2.0 * CGFloat.pi / CGFloat(N)
        var alpha = CGFloat.zero
        path.move(to: CGPoint(x: bounds.midX + R, y: bounds.midY))
        safePath.move(to: CGPoint(x: bounds.midX + safeRatio * R, y: bounds.midY))
        for _ in 0..<N-1 {
            alpha += da
            path.addLine(to: CGPoint(x: bounds.midX + R * cos(alpha),
                                     y: bounds.midY - R * sin(alpha)))
            safePath.addLine(to: CGPoint(x: bounds.midX + safeRatio * R * cos(alpha),
                                         y: bounds.midY - safeRatio * R * sin(alpha)))
        }
        path.close()
        safePath.close()
        path.lineWidth = 10.0
        path.stroke()
    }
    
}
