//
//  ChamberView.swift
//  JackJumper
//
//  Created by V.K. on 2/23/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ChamberView: UIView {
    
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        let boxColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        boxColor.setStroke()
        path.addArc(withCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: min(bounds.midX, bounds.midY) - 4.0, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        path.lineWidth = 10.0
        path.stroke()
    }
}
