//
//  ViewTwoController.swift
//  JackJumper
//
//  Created by V.K. on 2/25/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewTwoController: UIViewController {
    
    let jack = JackView()
    var size: CGFloat = 50.0
    
    @IBOutlet weak var chamberTwoView: ChamberTwoView!
    
    lazy var animator = UIDynamicAnimator(referenceView: chamberTwoView)
        
    override func viewDidLoad() {
        super.viewDidLoad()

        jack.frame = CGRect(x: chamberTwoView.bounds.midX - size * 0.5,
                            y: chamberTwoView.bounds.midY - size * 0.5,
                            width: size,
                            height: size)
        jack.isOpaque = false
        chamberTwoView.addSubview(jack)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(detectPan))
        jack.addGestureRecognizer(panGesture)
        
        let snap = UISnapBehavior(item: jack, snapTo: CGPoint(x: chamberTwoView.bounds.midX, y: chamberTwoView.bounds.midY))
        animator.addBehavior(snap)
               
    }
    
    @objc func detectPan(_ gesture: UIPanGestureRecognizer) {
        let origin = CGPoint(x: chamberTwoView.bounds.midX, y: chamberTwoView.bounds.midY)
        if let host = gesture.view {
            let newCenter = gesture.location(in: chamberTwoView)
            let isInside = chamberTwoView.safePath.contains(newCenter)
            if (isInside) {
                host.center = newCenter
            } else {
                let byBorder = pathClipsVector(path: chamberTwoView.safePath, origin: origin, target: newCenter)
                switch gesture.state {
                case .changed:
                    host.center = byBorder
                case .ended:
                    host.center = byBorder
                    //animator.updateItem(usingCurrentState: jack)
                default:
                    break
                }
            }
        }
    }
    
    private func pathClipsVector(path: UIBezierPath, origin: CGPoint, target: CGPoint) -> CGPoint {
        let dx = (target.x - origin.x) * 0.01
        let dy = (target.y - origin.y) * 0.01
        var currPoint = target
        while !path.contains(currPoint) {
            currPoint.x -= dx
            currPoint.y -= dy
        }
        return currPoint
    }
}
