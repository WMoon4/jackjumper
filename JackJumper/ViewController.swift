//
//  ViewController.swift
//  JackJumper
//
//  Created by V.K. on 2/22/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var jacks: [JackView] = []
    var size: CGFloat = 32.0
    
        
        
    @IBOutlet weak var chamberView: ChamberView!
    
    lazy var animator = UIDynamicAnimator(referenceView: chamberView)
    
    lazy var collisionBehavior: UICollisionBehavior = {
        let behavior = UICollisionBehavior()
        let path1 = UIBezierPath()
        path1.addArc(withCenter: CGPoint(x: chamberView.bounds.midX, y: chamberView.bounds.midY),
                     radius: min(chamberView.bounds.midX, chamberView.bounds.midY) - 7.0,
                     startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        behavior.addBoundary(withIdentifier: "roundChamber" as NSCopying, for: path1)
        //behavior.translatesReferenceBoundsIntoBoundary = true
        animator.addBehavior(behavior)
        return behavior
    }()
    
    lazy var itemBehavior: UIDynamicItemBehavior = {
        let behavior = UIDynamicItemBehavior()
        //behavior.allowsRotation = false
        behavior.elasticity = 1.01
        behavior.resistance = 0.0
        behavior.friction = 0.003
        animator.addBehavior(behavior)
        return behavior
    }()
    
    @IBAction func shakeItUp(_ sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            print("one tap")
            for jack in jacks {
                let push = UIPushBehavior(items: [jack], mode: .instantaneous)
                push.pushDirection = CGVector(dx: 0.25, dy: 0.0)
                push.action = { [unowned push, weak self] in
                    self?.animator.removeBehavior(push)
                }
                animator.addBehavior(push)
            }
                        
        default:
            break
        }
    }
       
    private func initializeJacks(num: Int) {
        var alpha: CGFloat = 0.0
        let da = 2.0 * CGFloat.pi / CGFloat(num)
        for _ in 0..<num {
            let jack = JackView()
            jack.frame = CGRect(x: chamberView.bounds.midX + 80.0 * cos(alpha) - size * 0.5,
                                y: chamberView.bounds.midY - 80.0 * sin(alpha) - size * 0.5,
                                width: size,
                                height: size)
            jack.isOpaque = false
            chamberView.addSubview(jack)
            collisionBehavior.addItem(jack)
            itemBehavior.addItem(jack)
            
            jacks += [jack]
            alpha += da
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeJacks(num: 12)
         
    }


}

